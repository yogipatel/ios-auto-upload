//
//  SyncManager.m
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import "AutoUploadManager.h"
#import "Constants.h"
#import "NSDate+Addtions.h"
#import "AssetsManager.h"
#import "MediaItem.h"

const NSUInteger kOneDayInSeconds = 86400;

@interface AutoUploadManager ()

@property (nonatomic, weak) AssetsManager *assetsManager;
@property (nonatomic, strong) NSTimer *scheduler;

@end

@implementation AutoUploadManager

- (instancetype) initWithPhotosManager:(AssetsManager *)manager
{
    self = [super init];
    if (self != nil) {
        self.assetsManager = manager;
        [self initializeBackupScheduler];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scheduleTimeDidChange) name:kNotificationScheduleBackupTimeChange object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performSync) name:kNotificationBackupNow object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationScheduleBackupTimeChange object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationBackupNow object:nil];
}

- (void)initializeBackupScheduler
{
    //1. Get the schedule backup date requested by user from user defaults.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *scheduleTimeAndDate = [defaults objectForKey:kScheduleBackupKey];
    if (scheduleTimeAndDate == nil) {
        scheduleTimeAndDate = [[NSDate date] dateByAddingTimeInterval:kOneDayInSeconds];
    }
    
    //2. Get time component from schedule date
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitHour | NSCalendarUnitMinute fromDate:scheduleTimeAndDate];
    NSUInteger scheduleHour = components.hour; //in 24hrs format in local timezone.
    NSUInteger scheduleMinute = components.minute;
    
    //3. Get time now components
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitHour | NSCalendarUnitMinute fromDate:[NSDate date]];
    NSInteger nowHour = components.hour; //in 24hrs format in local timezone.
    BOOL backupNow = NO;
    
    //4. Check whether the time now is before or after the schedule time. Remember days could have past between now and the schedule date.
    if (nowHour >= scheduleHour) {
        //Update the schedule date to tomorrow at the same scheduled time
        scheduleTimeAndDate = [[[NSDate date] dateWithHour:scheduleHour minute:scheduleMinute second:0] dateByAddingTimeInterval:kOneDayInSeconds];
        backupNow = YES;
    } else {
       //Update the schedule date to today at the schedule time
       scheduleTimeAndDate = [[NSDate date] dateWithHour:scheduleHour minute:scheduleMinute second:0];
    }
    
    //5. Save the new schedule date to db.
    [defaults setObject:scheduleTimeAndDate forKey:kScheduleBackupKey];
    [defaults synchronize];
    
    //6. Setup the timer.
    [self setNextTimerWithDate:scheduleTimeAndDate];
    
    if (backupNow) {
        //Perform a manual backup NOW!
        [self performSync];
    }
}

- (void)setNextTimerWithDate:(NSDate *)date
{
    self.scheduler = [[NSTimer alloc] initWithFireDate:date
                                            interval:0
                                            target:self
                                            selector:@selector(backupTimerFireMethod:)
                                            userInfo:nil
                                            repeats:NO];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:self.scheduler forMode: NSDefaultRunLoopMode];
}

- (void)backupTimerFireMethod:(NSTimer *)timer
{
    [self.scheduler invalidate];
    self.scheduler = nil;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self initializeBackupScheduler];
    });
}

- (void)updateBackupScheduler
{
    [self.scheduler invalidate];
    self.scheduler = nil;
    
    [self initializeBackupScheduler];
}

- (void)performSync
{
    //1. Get current assets from photos framework.
    NSMutableArray * currentMediaItems = [self.assetsManager getAssetMediaItems];
    
    //2. Get items we've backed up from DB
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *previousSyncedMediaItems = [defaults objectForKey:kBackedUpItemsKey];
    
    //3. Work out what's changed - basically find out only new items
    NSPredicate *relativeComplementPredicate = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", previousSyncedMediaItems];
    NSArray *relativeComplement = [currentMediaItems filteredArrayUsingPredicate:relativeComplementPredicate];
    
    //4. Queue up the new items to sync with server.
    for (MediaItem *mediaItem in relativeComplement) {
        
        //Add to Queue.
    }
    
    //5. Update the last sync time to now
    [defaults setObject:[NSDate date] forKey:kLastBackupKey];
}

#pragma mark -
- (void)scheduleTimeDidChange
{
    [self updateBackupScheduler];
}

@end
