//
//  MediaItem.h
//  AutoUpload
//
//  Created by Yogandra Patel on 20/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaItem : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSDate *creationDate;
@property (nonatomic, strong) NSDate *modifiedDate;

@end
