//
//  SyncManager.h
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AssetsManager;

@interface AutoUploadManager : NSObject

- (instancetype) initWithPhotosManager:(AssetsManager *)manager;

- (void)updateBackupScheduler;

@end
