//
//  PhotosManager.h
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/photos.h>

@interface AssetsManager : NSObject <PHPhotoLibraryChangeObserver>

- (NSMutableArray *)getAssetMediaItems;

@end
