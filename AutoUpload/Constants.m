//
//  Constants.m
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import "Constants.h"

NSString * const kLastBackupKey = @"last.backup";     // Last backup time from user defaults
NSString * const kScheduleBackupKey = @"schedule.backup"; // Last backup time from user defaults
NSString * const kBackedUpItemsKey = @"backup.items"; // previously backedup items from user defaults

NSString * const kNotificationScheduleBackupTimeChange = @"notification.schedule.time.changed";
NSString * const kNotificationBackupNow = @"notification.backup.now";