//
//  Constants.h
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef AutoUpload_Constants_h
#define AutoUpload_Constants_h

extern NSString * const kLastBackupKey;     // Last backup time from user defaults
extern NSString * const kScheduleBackupKey; // Last backup time from user defaults
extern NSString * const kBackedUpItemsKey; // previously backedup items from user defaults

extern NSString * const kNotificationScheduleBackupTimeChange;
extern NSString * const kNotificationBackupNow;

#endif
