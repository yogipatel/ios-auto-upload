//
//  AppDelegate.h
//  AutoUpload
//
//  Created by Yogandra Patel on 17/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

