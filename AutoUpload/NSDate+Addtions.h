//
//  NSDate+Addtions.h
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Addtions)

-(NSDate *) dateWithHour:(NSInteger)hour
                  minute:(NSInteger)minute
                  second:(NSInteger)second;

@end
