//
//  MediaItem.m
//  AutoUpload
//
//  Created by Yogandra Patel on 20/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import "MediaItem.h"

@implementation MediaItem

-(BOOL)isEqual:(MediaItem *)object
{
    BOOL result = [self.identifier isEqualToString:object.identifier] && [self.creationDate isEqualToDate:object.creationDate] && [self.modifiedDate isEqualToDate:object.modifiedDate];
    return result;
}

@end
