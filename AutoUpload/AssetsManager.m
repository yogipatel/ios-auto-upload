//
//  PhotosManager.m
//  AutoUpload
//
//  Created by Yogandra Patel on 18/08/2015.
//  Copyright (c) 2015 Yomagic. All rights reserved.
//

#import "AssetsManager.h"
#import "AutoUploadManager.h"
#import "Constants.h"
#import "MediaItem.h"

@interface AssetsManager ()

@property (strong) PHFetchResult *assetsFetchResults;
@property (strong) AutoUploadManager *syncManager;

@end

@implementation AssetsManager

- (instancetype)init
{
    self = [super init];
    if (self != nil) {
        [self initializeFetchResults];
        [self initializeSyncManager];
    }
    return self;
}

- (void)initializeFetchResults
{
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    self.assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
}

- (void)initializeSyncManager
{
    AutoUploadManager *manager = [[AutoUploadManager alloc] initWithPhotosManager:self];
    self.syncManager = manager;
}

- (NSMutableArray *)getAssetMediaItems
{
    NSMutableArray *array = [NSMutableArray new];
    MediaItem *mediaItem = nil;
    
    for (PHAsset *asset in self.assetsFetchResults) {
        
        //1. Ignore items that we are not interested in
        PHAssetMediaType mediaType = asset.mediaType;
        if (mediaType == PHAssetMediaTypeUnknown) {
            continue;
        }
        
        //2. Save the data we need.
        mediaItem = [MediaItem new];
        mediaItem.identifier = asset.localIdentifier;
        mediaItem.creationDate = asset.creationDate;
        mediaItem.modifiedDate = asset.modificationDate;
        
        //3. Add the identifer to the array.
        [array addObject:mediaItem];
    }
    return array;
}

#pragma mark -
- (void)photoLibraryDidChange:(PHChange *)changeInstance
{
    
}

@end
